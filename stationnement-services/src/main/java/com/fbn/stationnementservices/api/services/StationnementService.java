package com.fbn.stationnementservices.api.services;

import com.fbn.stationnementservices.api.DTO.PaiementDTO;
import com.fbn.stationnementservices.api.domain.Stationnement;
import com.fbn.stationnementservices.api.repository.StationnementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class StationnementService {
    @Autowired
    private StationnementRepository stationnementRepository;

    @Autowired
    private RestTemplate restTemplate;

    public PaiementDTO saveStationnement(Stationnement stationnement){
        PaiementDTO paiementDTO = new PaiementDTO();
        stationnementRepository.save(stationnement);
        paiementDTO.setNumeroPaiement(stationnement.getNumero());
        PaiementDTO paiementDTOResponse = restTemplate.postForObject("http://PAIEMENT-SERVICE/paiement/addPaiement",paiementDTO,PaiementDTO.class);

        return paiementDTOResponse;
    }

    public Stationnement findByNumero(String numero){
        return stationnementRepository.findByNumero(numero);
    }


}
