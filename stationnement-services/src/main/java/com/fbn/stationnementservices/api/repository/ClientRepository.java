package com.fbn.stationnementservices.api.repository;

import com.fbn.stationnementservices.api.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
}
