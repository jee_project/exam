package com.fbn.stationnementservices.api.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
public class Client {

    @Id
    @GeneratedValue
    private int id;

    private String numero;
    private String nom;
    private String prenom;
    private String adresse;
    private String cni;
    private String numeroPermis;
    private String num_immatriculation;
    private String num_carte_grise;

    @OneToMany(mappedBy = "client")
    @JsonBackReference
    private List<Stationnement> stationnements;
}
