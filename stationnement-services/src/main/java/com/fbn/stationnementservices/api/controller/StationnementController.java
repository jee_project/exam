package com.fbn.stationnementservices.api.controller;

import com.fbn.stationnementservices.api.DTO.PaiementDTO;
import com.fbn.stationnementservices.api.domain.Stationnement;
import com.fbn.stationnementservices.api.services.StationnementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stationnement")
public class StationnementController {
    @Autowired
    private StationnementService stationnementService;

    @PostMapping("/addStationnement")
    public PaiementDTO save(@RequestBody Stationnement stationnement){
        return  stationnementService.saveStationnement(stationnement);
    }

    @RequestMapping("/getSationnement/{numero}")
    public Stationnement getByNumero(@PathVariable String numero) {
        return stationnementService.findByNumero(numero);
    }

}
