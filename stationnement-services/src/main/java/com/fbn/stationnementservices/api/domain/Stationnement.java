package com.fbn.stationnementservices.api.domain;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
public class Stationnement {

    @Id
    @GeneratedValue
    private int id;
    private Double duree;
    private String numero;
    private String date_entree;
    private String Date_sortie;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "client_id")
    private Client client;

}
