package com.fbn.stationnementservices.api.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaiementDTO {

    private int id;
    private String numero;
    private Double montant;
    private String numeroPaiement;
}
