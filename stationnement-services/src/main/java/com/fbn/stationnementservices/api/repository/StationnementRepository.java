package com.fbn.stationnementservices.api.repository;

import com.fbn.stationnementservices.api.domain.Stationnement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationnementRepository extends JpaRepository<Stationnement, Integer > {
    public Stationnement findByNumero(String numero);
}
