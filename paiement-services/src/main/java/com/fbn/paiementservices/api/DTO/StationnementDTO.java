package com.fbn.paiementservices.api.DTO;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StationnementDTO {
    private int id;
    private Double duree;
    private String numero;
    private String date_entree;
    private String Date_sortie;
    private String numeroClient;
}
