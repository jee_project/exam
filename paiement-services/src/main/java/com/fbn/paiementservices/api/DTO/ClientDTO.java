package com.fbn.paiementservices.api.DTO;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClientDTO {

    private int id;
    private String numero;
    private String nom;
    private String prenom;
    private String adresse;
    private String cni;
    private String numeroPermis;
    private String num_immatriculation;
    private String num_carte_grise;
}
