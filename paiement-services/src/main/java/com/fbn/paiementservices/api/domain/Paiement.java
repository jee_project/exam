package com.fbn.paiementservices.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Paiement {
    @Id
    @GeneratedValue
    private int id;
    private String numero;
    private Double montant;
    private String numeroPaiement;
}
