package com.fbn.paiementservices.api.service;

import com.fbn.paiementservices.api.DTO.StationnementDTO;
import com.fbn.paiementservices.api.domain.Paiement;
import com.fbn.paiementservices.api.repository.PaiementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
public class PaiementService {
    @Autowired
    private PaiementRepository paiementRepository;

    public Paiement savePaiement(Paiement paiement){
        paiement.setNumero(UUID.randomUUID().toString());
        paiement.setMontant(1234.0);
        return paiementRepository.save(paiement);
    }
}
