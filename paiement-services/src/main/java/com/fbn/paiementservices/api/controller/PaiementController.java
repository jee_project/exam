package com.fbn.paiementservices.api.controller;

import com.fbn.paiementservices.api.domain.Paiement;
import com.fbn.paiementservices.api.service.PaiementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/paiement")
public class PaiementController {

    @Autowired
    private PaiementService paiementService;
    @PostMapping("/addPaiement")
    public Paiement save(@RequestBody Paiement paiement) {
        return paiementService.savePaiement(paiement);
    }

}
