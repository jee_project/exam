package com.fbn.gatewaye;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class HystrixFallbackController {
    @RequestMapping("/stationnementFallback")
    public Mono<String> stationnementServiceFallback(){
        return Mono.just("Timeout. Veuillez réessayer ultérieurement.");
    }
    @RequestMapping("/paiementFallback")
    public Mono<String> paiementServiceFallback(){
        return Mono.just("Timeout. Veuillez réessayer ultérieurement.");
    }
}
